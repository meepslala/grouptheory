#!/usr/bin/env python

SCRIPT_NAME = "binary"
SCRIPT_VERSION = "v0.2.7"
REVISION_DATE = "2015-03-08"
AUTHOR = "(dhavas@buffalo.edu)"
DESCRIPTION = """This script will take in symmetry matrices and output the classes that are present"""

###################################################################################################
# TASKS OF THIS SCRIPT:
# -to write script based on initial implementation of binary via excel
###################################################################################################

###################################################################################################
#TODO:
#     takein and store input and convert to matrix form
#
##################################################################################################

import sys
import os
import string
import math
import numpy as np
import copy

#############################################################################
#
# 1. Ask the user how many matrixes
# 2. Ask the user what size are the matrices
# 3. Retrieve operation information from user and assign to dictionary as key-value pair
# 4. Calculate X^-1YX for each matrix pair going in corresponding order starting with matrix 'A'
#    a. Store results in array
#    b. When calculation complete for each pair related to 'A' check to see what other matrices are present, these matrices make up a class
#    c. Determine what matrices remain, if any and repeat process
#
##############################################################################

########################################################
# 1 & 2

# num_matrix = int(raw_input('How many matrices are there?'))
n = 3 #int(raw_input('Enter number of rows in each matrix: '))
m = 3 #int(raw_input('Enter number of columns in each matrix: '))

########################################################
# 3. Retrieve operation information from user and assign to dictionary as key-value pair
#   a. create an dictionary size num_matrix
#   b. create for loop from 0 to num_matrix
#   c. in each position in the array place an empty matrix with n rows and m columns 
################################################################################
"""
Table_dict = {}
key_list = []
user = 'y'
while (user == 'y'):
    # get user input key (operation) and value (matrix represetnation of operation)
    input1 = raw_input('Please enter a variable  for a symmetry operation: ')
    input2 = raw_input ('Please enter the matrix representation in the form 1 2 3 4 5: ')

    # turn input2 into matrix form
    items = map(ast.literal_eval, input2.split(' '))
    assert(len(items) == n*m)
    matrix  = np.array(items).reshape((n,m))

    # enter data into dictionary
    Table_dict[input1] = matrix
    key_list.append(input1) 
    # ask user if there are more sym ops to add (y if yes n if no)
    user = raw_input('Is there another operation to enter (y for yes and n for no)? ')

########################################################
# 4. Calculate X^-1YX for each matric pair
#    a. get the first key vaue pair in the dictionary
#    b. calculate X^-1YX for each other value
#       1. check to see if same as any other value in dictionary (besides itself)
#          a. if no continue to next
#          b. if yes copy key to list and then continue
#    c. output list as class
#    d. repeat for all values not present in previous list
#
#########################################################    

#Tested with C4v and is not working, need to go inside and see what is happening
accountedFor_list = []
Class_list = []
#print key_list[0]"
for item in key_list:
    print 'here in key_list'
    if item not in accountedFor_list:
         class_list = []
         class_list.append(item)
         for key, value in Table_dict.items():
             compare = Table_dict.get(item)
             answer = np.dot(np.dot(np.linalg.inv(value),compare), value)
             if np.allclose(answer, compare) == False:
                 if np.allclose(answer, value) == True:
                     class_list.append(key)
    Class_list.append(class_list)

print Class_list
"""

## Testing to see why it won't work (classes not grouped: X^-1YX is not working appropriately)
testKey_list = ['E', 'C_4', 'C2_4', 'C3_4', 'Vx', 'Vy', 'D', 'Di']

test_dict = dict(E=np.matrix([[1.0, 0.0],[0.0, 1.0]]), C_4=np.matrix([[round(math.cos(math.pi/2),2), round(-math.sin(math.pi/2),2)],[round(math.sin(math.pi/2),2), round(math.cos(math.pi/2),2)]]), C2_4=np.matrix([[round(math.cos(2*math.pi/2),2), round(-math.sin(2*math.pi/2),2)],[round(math.sin(2*math.pi/2),2), round(math.cos(2*math.pi/2),2)]]), C3_4=np.matrix([[round(math.cos(3*math.pi/2),2), round(-math.sin(3*math.pi/2),2)],[round(math.sin(3*math.pi/2),2), round(math.cos(3*math.pi/2),2)]]), Vx=np.matrix([[-1.0, 0.0],[0.0, 1.0]]), Vy=np.matrix([[1.0, 0.0],[0.0, -1.0]]), D=np.matrix([[0.0, 1.0],[1.0, 0.0]]), Di=np.matrix([[0.0, -1.0],[-1.0, 0.0]]))

Class_list=[]
accountedFor_list = []

for item in testKey_list:
    if item not in accountedFor_list:
        class_list = [item]
        accountedFor_list.append(item)
        for key, value in test_dict.items():
                compare = test_dict.get(item)
                answer = np.linalg.inv(value) * compare * value
                if np.allclose(answer, compare) == False:
                    for key2, value2 in test_dict.items():
                        if np.array_equiv(value2, answer) == True and key2 not in accountedFor_list:
                            class_list.append(key2)
                            accountedFor_list.append(key2)
        Class_list.append(class_list)

print Class_list
